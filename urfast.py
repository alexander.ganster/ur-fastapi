# import uvicorn websever
import uvicorn
# import FastAPI and Request for RESTApi
from fastapi import FastAPI, Body, Request
# import RedirectResponse for redirecting to a different URL
#from starlette.responses import RedirectResponse, Response

from typing import Optional
# import prometheus exporter
from prometheus_fastapi_instrumentator import Instrumentator
# import prometheus webserver
from prometheus_client import start_http_server
# import pandas for parsing CSV
import pandas
# import StringIO; needed as input for pandas.read_csv()
from io import StringIO
# for sorting of lists by dict values
from operator import itemgetter


# create a FastAPI instance; set root path as docs-url
app = FastAPI(docs_url="/")


# use the Request class from Starlette for processing the request: https://www.starlette.io/requests/
# allows us to go retrieve connection details from HTTP, TCP and IP layers.
# details here: https://levelup.gitconnected.com/4-useful-advanced-features-in-fastapi-f08e4db59637
# this method is async
@app.post("/contact-sort")
async def contact_sort(request: Request):
    # wait until the request body is fully transferred using the await statement.
    # and convert from list of bytes to str
    input = ( await request.body() ).decode('utf-8')
    #print(input)

    # create a pandas dataframe from the input-str
    # read_csv() only accepts IO objects, so we have to encapsulate it
    # our input doesn't have a header; 
    # csv fields should be read as "first_name,last_name"
    df = pandas.read_csv( StringIO(input), header=None, names=["first_name", "last_name"] )

    # change order of the columns
    df = df.reindex(columns=["last_name", "first_name"])

    # sort the dataframe by last_name then last_name
    df = df.sort_values(["last_name", "first_name"])
    #df = df[["last_name", "first_name"]] # alternative sorting method

    # print the dataframe
    print(df)

    # generate dict from dataframe
    df_dict = df.to_dict(orient="records")
    # print(df_dict)
    
    # return the genenerated dict
    return df_dict


# use the Request class from Starlette for processing the request: https://www.starlette.io/requests/
# allows us to go retrieve connection details from HTTP, TCP and IP layers.
# details here: https://levelup.gitconnected.com/4-useful-advanced-features-in-fastapi-f08e4db59637
# this method is async; if we want to retrieve the body we have to wait until it is fully transferred
# using the await statement.
@app.post("/contact-sort2")
async def contact_sort2(request: Request):
    # wait until the request body is fully transferred using the await statement.
    # and convert from list of bytes to str
    input = ( await request.body() ).decode('utf-8')

    contacts = []
    # split the csv by lines
    for line in input.splitlines():
        # split each line by comma
        entry = line.split(',')
        # create a dict; first CSV value is 'first_name', second value is 'last_name'
        # append the dict to contacts list
        contacts.append( dict({
            "last_name": entry[1],
            "first_name": entry[0]
         } ))

    # sort contacts by last_name then first_name
    contacts_sorted = sorted(contacts, key=itemgetter('last_name','first_name') )
    # print(contacts_sorted)

    return contacts_sorted


if __name__ == "__main__":
    # expose metrics; if 'include_in_schema' is True metrics endpoints are exposed in the API as well
    Instrumentator().instrument(app).expose(app, include_in_schema=True, should_gzip=True)

    # start the prometheus http server
    start_http_server(8000)
    
    # start the http server
    uvicorn.run(app, host="0.0.0.0", port=8080)
