FROM python:3.9-slim
RUN mkdir -p /ur-fastapi
WORKDIR /ur-fastapi
COPY urfast.py /ur-fastapi/
#RUN apk add py-pandas py3-prometheus-client
RUN pip3 install fastapi uvicorn pandas prometheus_fastapi_instrumentator prometheus-client
EXPOSE 8080
EXPOSE 8000
CMD ["python3", "/ur-fastapi/urfast.py"]
