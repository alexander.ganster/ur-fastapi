# REST Webservice
Starts a REST webservice based on Python using the FastAPI framework.

Can be tested with:

    # curl -X 'POST' \
           'http://localhost:8080/contact-sort' \
           -H 'accept: application/json' \
           -H 'Content-Type: text/csv' \
           --data-binary @names2.csv

Sample test data is located in 'tests/sample-data/'.

## Kubernetes Deployment

To deploy the REST service on Kubernetes:

First create a secret for pulling from a private docker registry:

    # kubectl -n ur-fastapi create secret docker-registry imagepullsecret-gitlab.com --docker-server=registry.gitlab.com --docker-username=YOUR_USER --docker-password=YOUR_PASSWORD --docker-email=YOUR_EMAIL

Create the Kubernetes deployment:

    # kubectl -n ur-fastapi apply -f k8s/deployment.yaml

Adapt the ingress definitions in the file "k8s/ingress.yaml" according to your environment and deploy it:

    # kubectl -n ur-fastapi apply -f k8s/ingress.yaml
